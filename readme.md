## Details
This program collects all users and their passwords of a specific email domain and output to a file. Make sure you delete output file once it is not needed.

## Requirements:
Needs to be run on Webair's maildb server or Webair hosted client's mailserver. Thus copy the script to appropriate mail server and execute there.

"vdominfo" command is needed for this program to run

## Usage
```
python maildb.py domainname

Eg:
python maildb.py galaxy.com

Result: 
bjoshi1@galaxy.com:sorry
bjoshi2@galaxy.com:idontknow

```
## Author
Binaya Joshi
