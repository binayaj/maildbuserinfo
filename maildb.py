#!/usr/bin/env python
# Author: Binaya Joshi
# vdominfo command needs to be present on the system.
# This script collects all users and passwords of a specific email domain and output to a file

import subprocess,time,os,sys

def process_domains():
 file1=(str(int(time.time())))
 with open (file1,"a+") as fp:
  p1=subprocess.call (['vdominfo |grep domain:|cut -d " " -f 2'],shell=True,stdout=fp)
  fp.seek(0)
  for j in fp:
          domains(j)
  os.remove (file1)

def domains(i):
  try:
      domain=i.rstrip()
      p2=subprocess.Popen (('vdominfo',domain),stdout=subprocess.PIPE)
      p3=subprocess.check_output(('grep','dir'),stdin=p2.stdout)
      path=(p3.split())
      p4=subprocess.Popen(('find',path[1],'-type', 'd','-mindepth','1'),stdout=subprocess.PIPE)
      p5=subprocess.Popen(('grep','-vs','Maildir'),stdin=p4.stdout,stdout=subprocess.PIPE)
      p6=subprocess.Popen(('grep','-vs','postmaster'),stdin=p5.stdout,stdout=subprocess.PIPE)
      result=p6.communicate()[0]
      if len(result) > 0:
         for user in result.split("\n"):
              if len(user)>0:
               email=user.split("/")[-1]+"@"+domain
               p7=subprocess.Popen (('vuserinfo',email),stdout=subprocess.PIPE)
               p8=subprocess.Popen(('grep','clear'),stdin=p7.stdout,stdout=subprocess.PIPE)
               p9=subprocess.Popen(('awk','{print $3}'),stdin=p8.stdout,stdout=subprocess.PIPE)
               result1=p9.communicate()[0]
               user_cred=email+":"+result1.rstrip();
               print (user_cred)

  except:
    print "Seems domain is " +i+" not present"
def main():
 if (len(sys.argv) > 1):
  for i in sys.argv[1:]:
          domains(i)
 else:
   process_domains()

if __name__=="__main__":
      main()

